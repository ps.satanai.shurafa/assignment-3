public class Node {
    private Integer data;
    private Node left,right;
    Node(Integer value){
        data = value;
        left = new Node();
        right = new Node();
    }

    Node(){
        data = null;
    }

    public Integer getData(){
        return data;
    }

    public Node getRight(){
        return right;
    }

    public Node getLeft(){
        return left;
    }

    public boolean isNull(Integer value){
        return value == null;
    }
    public boolean isValueSmaller(Integer value){
        return value < this.getData();
    }
    public boolean isValueGreater(Integer value){
        return value > this.getData();
    }

    public boolean accept(Integer value){
         if (isValueGreater(value)) {
            if(isNull(right.data)){
                right = new Node(value);
                return true;
            }
            else if (!isNull(right.data)) {
                return right.accept(value);
            }
        }

        else if (isValueSmaller(value)) {
            if(isNull(left.data)){
                left = new Node(value);
                return true;
            }
            else if (!isNull(left.data)) {
                return left.accept(value);
            }
        }
        return false;
    }

}
