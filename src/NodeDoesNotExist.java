public class NodeDoesNotExist extends RuntimeException{
    public NodeDoesNotExist() {
        super("NodeDoesNotExist");
    }
}
