import java.util.ArrayList;
import java.util.Scanner;

public class BST {
    private Node root,currentNode;
    private ArrayList<Integer> nodes = new ArrayList<>();
    private Integer counter,nodeDepth;
    private boolean isRoot,isCurrentNodeInit;

    public BST(){
        currentNode = new Node();
        isRoot = true;
        isCurrentNodeInit = false;
        counter = 0;
        nodeDepth = -1;
    }
    public boolean accept(Integer value) throws NodeAlreadyExist{
        if(isRoot){
            root = new Node(value);
            addNode(value);
            isRoot = false;
            return true;
        }
        else if(nodeExist(value)){
            throw new NodeAlreadyExist();
        }
        else if (root.accept(value)) {
            addNode(value);
            return true;
        }
        return false;
    }

    public Integer treeDepth(){
        return nodes.size() / 2;
    }
    public void reInitialize(){
        isCurrentNodeInit = false;
        counter = 0;
    }
    public void addNode(Integer value){
        nodes.add(value);
    }

    public boolean nodeFounded(Integer value){
        return value == currentNode.getData();
    }

    public boolean nodeExist(Integer value){
        return nodes.contains(value);
    }

    public boolean isValueGreater(Integer value){
        return value > currentNode.getData();
    }

    public boolean isValueSmaller(Integer value){
        return value < currentNode.getData();
    }

    public void moveRight(){
        currentNode = currentNode.getRight();
    }

    public void moveLeft(){
        currentNode = currentNode.getLeft();
    }

    public Integer depth(Integer value) throws NodeDoesNotExist{
        if(!isCurrentNodeInit){
            currentNode = root;
            isCurrentNodeInit = true;
        }
        if(!nodeExist(value)){
            throw new NodeDoesNotExist();
        }
        else if(nodeFounded(value)){
            nodeDepth = counter;
            reInitialize();
        } else if (isValueGreater(value)) {
            counter++;
            moveRight();
            depth(value);
        } else if (isValueSmaller(value)) {
            counter++;
            moveLeft();
            depth(value);
        }
        return nodeDepth;
    }
    public static void main(String[] args){
        BST binaryTree = new BST();
        Scanner sc = new Scanner(System.in);

        for(int i=0; i<6; i++){
            Integer value = sc.nextInt();
            binaryTree.accept(value);
        }
        System.out.println("The tree depth = "+binaryTree.treeDepth());

    }
}
